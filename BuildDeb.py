#!/usr/bin/env python3
"""Builds packages"""
import sys
from pathlib import Path
from prebuilder.systems import CMake, Make
from prebuilder.distros.debian import Debian
from prebuilder.buildPipeline import BuildPipeline, BuildRecipy
from prebuilder.repoPipeline import RepoPipelineMeta
from prebuilder.core.Package import PackageMetadata
from prebuilder.core.Package import PackageRef, VersionedPackageRef
from prebuilder.fetchers.GitRepoFetcher import GitRepoFetcher
from fsutilz import movetree, copytree
from ClassDictMeta import ClassDictMeta

thisDir = Path(".").absolute()


class build(metaclass=RepoPipelineMeta):
	"""It's {maintainerName}'s repo for {repoKind} packages of build tools."""
	
	DISTROS = (Debian,)
	
	def waxeye():
		repoURI = "https://github.com/waxeye-org/waxeye"
		
		def installRule(sourceDir, buildDir, pkg, gnuDirs):
			copytree((sourceDir / "bin"), pkg.nest(gnuDirs.bin))
			copytree((sourceDir / "lib"), pkg.nest(gnuDirs.lib))
		
		class cfg(metaclass=ClassDictMeta):
			descriptionShort = "a parser generator based on parsing expression grammars (PEGs) for C, Java, JavaScript, Python, Racket, and Ruby."
			descriptionLong = """Waxeye is a parser generator based on parsing expression grammars (PEGs). It supports C, Java, Javascript, Python, Ruby and Scheme."""
			license = "MIT"
			section = "devel"
			homepage = repoURI
		
		bakeBuildRecipy = BuildRecipy(Make, GitRepoFetcher(repoURI, refspec="master"), buildOptions = {}, useKati=False, configureScript=None, installRule=installRule, makeArgs=["compiler"])
		bakeMetadata = PackageMetadata("waxeye", **cfg)
		
		return BuildPipeline(bakeBuildRecipy, ((Debian, bakeMetadata),))


if __name__ == "__main__":
	build()
